package info.rays3t.auttcb.exception;

public class TS3BotApiNotReadyException extends RuntimeException {
    public TS3BotApiNotReadyException(String message) {
        super(message);
    }
}
