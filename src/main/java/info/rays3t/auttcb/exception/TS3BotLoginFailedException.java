package info.rays3t.auttcb.exception;

public class TS3BotLoginFailedException extends TS3BotException {
    public TS3BotLoginFailedException(String message) {
        super(message);
    }
}
