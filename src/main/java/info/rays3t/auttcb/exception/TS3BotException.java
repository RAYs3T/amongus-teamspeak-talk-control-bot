package info.rays3t.auttcb.exception;

public abstract class TS3BotException extends Exception {
    public TS3BotException(String message) {
        super(message);
    }
}
