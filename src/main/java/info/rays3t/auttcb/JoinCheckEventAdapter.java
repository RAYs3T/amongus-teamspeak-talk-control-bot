package info.rays3t.auttcb;

import com.github.theholywaffle.teamspeak3.api.event.ClientMovedEvent;
import com.github.theholywaffle.teamspeak3.api.event.TS3EventAdapter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
public class JoinCheckEventAdapter extends TS3EventAdapter {
    private final static Logger log = LogManager.getLogger(JoinCheckEventAdapter.class);

    @Value("{tsbot.target-channel-id")
    private Integer targetChannelId;

    @Value("{tsbot.group.base-group-id}")
    private Integer tsBotBaseGroupId;

    @Value("{tsbot.group.muted-group-id}")
    private Integer tsBotMutedGroupId;

    @Autowired // Yes this is deprecated, but there is currently no solution for constructor based init using lazy init
    @Lazy
    private TS3ServerService serverService;

    @Override
    public void onClientMoved(ClientMovedEvent event) {
        log.trace("onClientMoved event received by JoinCheckEventAdapter Invoker=" + event.getInvokerId()
                + " client=" + event.getClientId());
        if (event.getTargetChannelId() == targetChannelId) {
            serverService.assignGroup(tsBotBaseGroupId, event.getClientId());
            log.info("Assigned group " + tsBotBaseGroupId + " to " + event.getTargetChannelId());
        }
    }
}
