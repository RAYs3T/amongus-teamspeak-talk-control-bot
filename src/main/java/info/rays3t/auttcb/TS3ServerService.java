package info.rays3t.auttcb;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.exception.TS3CommandFailedException;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import info.rays3t.auttcb.exception.TS3BotLoginFailedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class TS3ServerService {
    private static Logger log = LogManager.getLogger(TS3ServerService.class);

    private final Object connectLock = new Object();
    private TS3Api api;
    private TS3Query query;

    private JoinCheckEventAdapter joinCheckEventAdapter;

    public TS3ServerService(JoinCheckEventAdapter joinCheckEventAdapter) {
        this.joinCheckEventAdapter = joinCheckEventAdapter;
    }

    @Value("{tsbot.query.address}")
    private String tsBotQueryAddress;

    @Value("{tsbot.query.port}")
    private Integer tsBotQueryPort;

    @Value("{tsbot.vserver.port}")
    private Integer tsBotVServerPort;

    @Value("{tsbot.query.user}")
    private String tsBotQueryUser;

    @Value("{tsbot.query.nickname}")
    private String tsBotNickname;

    @Value("{tsbot.query.password}")
    private String tsBotQueryPassword;

    @Value("{tsbot.query.rate-limit}")
    private Integer tsBotQueryRateLimit;


    public void initialize() throws TS3BotLoginFailedException {
        connect();
    }

    private void connect() throws TS3BotLoginFailedException {

        synchronized (connectLock) {
            if (query != null && query.isConnected()) {
                log.warn("Skipping connect, Query is connected!");
                return;
            }
            log.info("Establishing connection to teamspeak: " + tsBotQueryAddress + ":" + tsBotQueryPort + " ...");
            // Create config to set host
            TS3Config configuration = new TS3Config();
            configuration.setHost(tsBotQueryAddress);
            configuration.setQueryPort(tsBotQueryPort);

            // If teamspeak is use with default settings, we need to back up on the fetch frequency...
            configuration.setFloodRate(TS3Query.FloodRate.custom(tsBotQueryRateLimit));

            // Use config to create new API connection
            query = new TS3Query(configuration);
            api = query.getApi();

            query.connect();

            try {
                api.login(tsBotQueryUser, tsBotQueryPassword);
            } catch (TS3CommandFailedException ex) {
                log.fatal("Unable to login at '" + tsBotQueryAddress + ":" + tsBotVServerPort + " using loginName='"
                        + tsBotQueryUser + "': " + ex.getMessage());
                throw new TS3BotLoginFailedException("loginName: " + tsBotQueryUser);
            }

            api.selectVirtualServerByPort(tsBotVServerPort);
            try {
                api.setNickname(tsBotNickname);
            } catch (TS3CommandFailedException ex) {
                log.warn("Unable to set bots nickname to: '" + tsBotNickname +
                        "'. This can be caused when the query user was created by an actual person instead of the query account. " +
                        "This could cause further problems since the user has the same groups as the bot. " +
                        "internal error message: " + ex.getMessage());
            }

            setupListener();
            log.info("Connected to TS3Server '" + tsBotQueryAddress);
        }
    }

    private void setupListener() {
        log.trace("Setting up listeners ...");
        // Let the server know we want to receive events
        api.registerAllEvents();
        // and register our listeners
        api.addTS3Listeners(joinCheckEventAdapter);
    }

    public void assignGroup(int groupId, int clientId) {
        for (Client c : api.getClients()) {
            if (c.getId() == clientId) {
                api.addClientToServerGroup(groupId, c.getDatabaseId());
                return;
            }
        }
        log.error("Unable to find client with id: " + clientId + " - Client may left.");
    }

}
