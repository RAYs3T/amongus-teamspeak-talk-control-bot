package info.rays3t.auttcb;

import info.rays3t.auttcb.exception.TS3BotLoginFailedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BotApplication {
    private static final Logger log = LogManager.getLogger(BotApplication.class);
    private TS3ServerService server;

    public static void main(String[] args) {
        SpringApplication.run(BotApplication.class, args);
    }

    public BotApplication(@Autowired TS3ServerService tsServer) {
        this.server = tsServer;
    }

    public void run(String[] args) {
        try {

            server.initialize();
        } catch (TS3BotLoginFailedException e) {
            log.error(e.getMessage());
            System.exit(1);
        }
    }

}
