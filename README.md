# AmongUs Teamspeak Talk Control Bot

Takes care of group management in TeamSpeak to ensure people in Among Us do not talk when they should not talk. The bot ensures that only people in speciffic channel have a speciffic role that is needed for this setup.

## Prerquisites
This is done by having 2 groups on the TeamSpeak server:
A `Among us` group and a `Among us muted` group. Both groups should not be permanent (`b_group_is_permanent`) and the `Among us muted` should have the `i_client_talk_power` set to -1 **AND** Negate. 


To use these groups a user has to have the needed permissions to add and remove people to the `Among us muted` group. The user has to make a hotkey in TeamSpeak that looks like this:

![image.png](./image.png)

where the left is the key it is bound to (In my case F24) and the right is the `Action` > `Permissions` > `Toggle Server Group` > `<Player group name>;<Mute player group name>`



## Known issues
- If someone has the `Among us muted` group and others don't, pressing the hotkey will swap the states of both players instead of muting everyone or unmiting everyone. But since the groups are not permanent, and the group should never be assigned manually this should not occur.
- There can only be one Among us channel / lobby on a TeamSpeak Server. This can be expanded by making 4 roles or 2 for each channel and running the bot once per channel or rewriting the bot.

## Tips
- To mute the sounds of getting and losing the server group you can disable them in the settings under Notifications:

![alt text](https://i.imgur.com/RuMpXTB.png "The settings to disable the sounds")
- To make hotkeys work from everywhere even while in another window you have to start TeamSpeak as an administrator.
